import React, { useState } from 'react';
import {Text, TouchableOpacity, View, Image, TextInput, StyleSheet} from 'react-native'

const otpScreen = ({navigation, route}) => {

    const[activatebtn, setActivateBtn]  = useState(true)

    const { otp , phoneNumber} = route.params;
    console.log(otp)
    const[num1, setNum1] = useState(0);
    const[num2, setNum2] = useState(0);
    const[num3, setNum3] = useState(0);
    const[num4, setNum4] = useState(0);
    const[num5, setNum5] = useState(0);
    const[num6, setNum6] = useState(0);

    const [f1, setf1] = useState(false)
    const [f2, setf2] = useState(false)
    const [f3, setf3] = useState(false)
    const [f4, setf4] = useState(false)
    const [f5, setf5] = useState(false)
    const [f6, setf6] = useState(false)

    const [input1Ref, setInput1Ref] = useState();
    const [input2Ref, setInput2Ref] = useState();
    const [input3Ref, setInput3Ref] = useState();
    const [input4Ref, setInput4Ref] = useState();
    const [input5Ref, setInput5Ref] = useState();
    const [input6Ref, setInput6Ref] = useState();

    const goBack = () => {
        navigation.goBack();
    }

    const resendFunction = () => {
        alert('resend OTP')
        console.log(otp)
    }

    const confirmOtp = () => {
        if(otp.toString() === `${num1}${num2}${num3}${num4}${num5}${num6}`){
            navigation.navigate('languagescreen')
        }else{
            alert('incorrect otp')
        }
    }

    return(
        <View style={styles.main}>
            <TouchableOpacity style={{marginBottom: 40}} onPress={() => goBack()}>
                <Image source={require('./../../images/back.png')} resizeMode='contain' style={styles.backButton}/>
            </TouchableOpacity>
            <Text style={{textAlign: 'center', fontSize: 23, fontWeight: 'bold',color:'#121212'}}>Enter your OTP</Text>
            <Text style={{textAlign: 'center', marginTop: 10, color:'#606060'}}>We sent a 6-digit code to 9178260066</Text>
            <View style={styles.otpBox}>
                    <TextInput
                        editable
                        multiline={false}
                        ref={(input) => setInput1Ref(input)}
                        keyboardType='numeric'
                        style={f1 ? styles.textInpFocus : styles.textInp}
                        onChangeText={(text) => (setNum1(text),input1Ref.blur(), input2Ref.focus())}
                        onFocus={() => setf1(true)}
                        onBlur={() => setf1(false)}
                    />
                    <TextInput
                        editable
                        multiline={false}
                        keyboardType='numeric'
                        style={f2 ? styles.textInpFocus : styles.textInp}
                        ref={(input) => setInput2Ref(input)}
                        onChangeText={(text) => (setNum2(text) ,input2Ref.blur(), input3Ref.focus())}
                        onFocus={() => setf2(true)}
                        onBlur={() => setf2(false)}

                    />
                    <TextInput
                        editable
                        multiline={false}
                        keyboardType='numeric'
                        ref={(input) => setInput3Ref(input)}
                        style={f3 ? styles.textInpFocus : styles.textInp}
                        onChangeText={(text) => (setNum3(text),input3Ref.blur(), input4Ref.focus())}
                        onFocus={() => setf3(true)}
                        onBlur={() => setf3(false)}
                    />
                    <TextInput
                        editable
                        multiline={false}
                        keyboardType='numeric'
                        style={f4 ? styles.textInpFocus : styles.textInp}
                        onFocus={() => setf4(true)}
                        onBlur={() => setf4(false)}
                        ref={(input) => setInput4Ref(input)}
                        onChangeText={(text) => (setNum4(text), input4Ref.blur(), input5Ref.focus())}

                    />
                    <TextInput
                        editable
                        multiline={false}
                        keyboardType='numeric'
                        ref={(input) => setInput5Ref(input)}
                        onChangeText={(text) => (setNum5(text), input5Ref.blur(), input6Ref.focus())}
                        style={f5 ? styles.textInpFocus : styles.textInp}
                        onFocus={() => setf5(true)}
                        onBlur={() => setf5(false)}
                    />
                    <TextInput
                        editable
                        multiline={false}
                        keyboardType='numeric'
                        ref={(input) => setInput6Ref(input)}
                        onChangeText={(text) => (setNum6(text), input6Ref.blur(), setActivateBtn(false))}
                        style={f6 ? styles.textInpFocus : styles.textInp}
                        onFocus={() => setf6(true)}
                        onBlur={() => setf6(false)}

                    />
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 30}}>
                <Text style={{fontSize: 16}}>Didn't receive the OTP?</Text>
                <TouchableOpacity onPress={() =>  resendFunction()}>
                    <Text style={{color: 'red', marginStart: 5, fontSize: 16}}>RESEND</Text>
                </TouchableOpacity>
            </View>

            <TouchableOpacity style={activatebtn ? styles.confirmButtonDeActivated : styles.confirmButtonActivated} disabled={activatebtn} onPress={() => confirmOtp()}>
                <Text style={{color: '#fff', fontSize: 16}}>C O N F I R M</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    main:{
        flex: 1,
        marginVertical: 20,
        marginHorizontal: 20
    },
    backButton:{
        height: 20,
        width: 20
    },
    otpBox:{
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 40
    },
    textInp:{
        marginHorizontal: 5,
        width: 50,
        borderRadius: 10,
        borderWidth: 1.544,
        borderColor: '#c0c0c0',
        fontSize: 25,
        textAlign: 'center',
        fontWeight: 'bold'

    },
    textInpFocus:{
        marginHorizontal: 5,
        width: 50,
        borderRadius: 10,
        borderWidth: 1.544,
        borderColor: '#c51315',
        fontSize: 25,
        textAlign: 'center',
        backgroundColor: '#f8f8f8',
        fontWeight: 'bold'
    },
    confirmButtonActivated:{
        backgroundColor: '#c51315',
        alignItems: 'center',
        paddingVertical: 15,
        borderRadius: 10,
        marginTop: 50
    },
    confirmButtonDeActivated:{
        backgroundColor: 'rgba(197,19,21, 0.5)',
        alignItems: 'center',
        paddingVertical: 15,
        borderRadius: 10,
        marginTop: 50,
        
    }
})

export default otpScreen