import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
} from 'react-native';

import * as Progress from 'react-native-progress';

const PlayerScreen =({navigation}) =>  {
  const [calls, state ] = useState( [

        {id:1,  description: "Lorem ipsum is an script and is tudesnjsdbc",   episodetitle:"10. Episode Title", status:"Today", image:"https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png"},
        {id:2,  description: "Lorem ipsum is an script and is tudesnjsdbc",   episodetitle:"172. Episode Title",status:"Today", image:"https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png"} ,
        {id:3,  description:"Lorem ipsum is an script and is tudesnjsdbc",  episodetitle:"06. Episode Title",status:"Today", image:"https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png"} ,
        {id:4,  description:"Lorem ipsum is an script and is tudesnjsdbc",  episodetitle:"06. Episode Title",status:"Today", image:"https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png"} ,
        {id:5,  description:"Lorem ipsum is an script and is tudesnjsdbc",  episodetitle:"06. Episode Title",status:"Today", image:"https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png"} ,
        {id:6,  description:"Lorem ipsum is an script and is tudesnjsdbc",  episodetitle:"06. Episode Title",status:"Today", image:"https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png"} ,
       
      ]);
  
      



    return(
      <View style={styles.container} >
      
        <View style={styles.container1}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image style={styles.back}  source={require('./../../images/backblack.png')} resizeMode='contain'/>
        </TouchableOpacity>
   </View>
   <View  style={styles.container2}>
         </View>
<View style={styles.overlap} >
        <Image style={styles.poster}  source={{uri:'https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png'}} />
        <TouchableOpacity >
        <Image style={styles.share}  source={require('./../../images/share.png')} resizeMode='contain'/>
        </TouchableOpacity>
    
        <View style={styles.subscribe}>
        <Text style={styles.subscribetxt} >Subscribe</Text>
        <Image  style={styles.subscribe} source={require('./../../images/subscribe.png')} resizeMode='contain'/>
            

</View>
        </View>
           
     <View  style={styles.container2}>
         <Text style={styles.text1}>
         The Drop Out
         </Text>
         <Text style={styles.text2}>
         Studio 71
         </Text>
         <Text style={styles.text3}>
         Lorem Ipsum has been the industry's standard at dummy text ever since the 1500
         </Text>

         </View>
    
     
       
        <FlatList 
          extraData={state}
          data={calls}
          keyExtractor = {(item) => {
            return item.id;
          }}
          renderItem={ ({item}) => {
            return (
              
                <View style={styles.row}>
                  <Image source={{ uri: item.image }} style={styles.pic} />
                  <View>
                  <View style={styles.msgContainer}>
                      <Text style={styles.msgTxt}>{item.status}</Text>
                    </View>
                    <View style={styles.msgContainer}>
                      <Text style={styles.msgTxt2}>{item.episodetitle}</Text>
                    </View>
                 
                    <View style={styles.nameContainer}>
                   
                    <Text style={styles.nameTxt} numberOfLines={1} ellipsizeMode="tail">{item.description}</Text>

<TouchableOpacity>
<Text style={styles.mblTxt}>more</Text>
</TouchableOpacity>
                    </View>
                    <Progress.Bar progress={0.3} width={250} color={'#C51315'} borderRadius={0} style={styles.prog} />
                  </View>
                </View>
            
            );
          }}
          ItemSeparatorComponent={() => {  
            return (  
                <View  
                    style={{  
                        height: 23,  
                        width: "100%",  
                        backgroundColor: "#f6f6f6",  
                    }}  
                />  
            );  
        } }  />
      </View>
    );
  
}

const styles = StyleSheet.create({
  container: {
  flex:1,
  backgroundColor:'#fff'
  },
  share: {
 
  position:'absolute',
  marginLeft:180,
  marginTop:40,
  height:50,
  width:50,
  
  
  },
  subscribetxt: {
      marginLeft:10,
 fontSize:20,
 color:'white',
 marginTop:3,
    
    
    },
  prog:{
      marginLeft:12,

  },
  subscribe: {
    backgroundColor:'#C51315',
    position:'absolute',
    marginLeft:250,
    marginTop:48,
    height:43,
    width:120,
    borderRadius:8,
    
    
    
    },
  poster: {
      height:120,
      width:130,
      marginTop:10,
    borderRadius:10,
    position:'absolute',
    
    },  overlap: {
        flexDirection:'row',
        flexBasis:'1%',
      position:'absolute',
      marginLeft:17,
      marginTop:60,
      },
  container1: {
    flexBasis:'20%',
    backgroundColor:'#F0D3EA'
    },
    text1: {
    
        fontWeight:'bold',
        fontSize:24,
       marginLeft:20,
        },
        text2: {
            marginLeft:20,
            },
            text3: {
                fontSize:15,
                marginLeft:20,
                },
                
    container2: {
        alignItems:'flex-start',

        flexDirection:'column',
        flexBasis:'16%',
        backgroundColor:'#FBFBFB'
        },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#868F8F',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    padding: 10,
  },
  notify: {
    flex:0.28,
    flexDirection:'row',
    backgroundColor: '#D11515',
    
  },
  timecontainer:{
   justifyContent:'flex-start'
    
  },
  item: { 
    marginLeft:75,
    marginTop: 9,
    textAlign: 'center',
    alignItems:'center',
    paddingTop: 6,
    color: 'white',
    fontSize: 23,
   
},
back:{
  marginTop:10,
  marginLeft:18,
  height: 37,
  width: 37,

 },
  pic: {
    borderRadius: 10,
    width: 80,
    height: 80,
  
  },
  nameContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 300,
  },
  nameTxt: {
    marginLeft: 15,
    fontWeight: '600',
    color: '#222',
    fontSize: 18,
    width:230,
  },
  mblTxt: {
    fontWeight: '500',
    color: 'red',
    fontSize: 17,
  },
  msgContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  msgTxt: {
    fontWeight: 'bold',
    fontFamily:"sans-serif",
    color: 'red',
    fontSize: 14,
    marginLeft: 15,
  },
  msgTxt2: {
    fontWeight: 'bold',
    fontFamily:"sans-serif",
    color: 'black',
    fontSize: 18,
    marginLeft: 15,
  },
});
export default PlayerScreen;