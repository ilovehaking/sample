import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Alert,Image
} from 'react-native';


const Data = [
  {
    id: 1,
    first_name: 'Telugu',
  },
  {
    id: 2,
    first_name: 'English',
  },
  {
    id: 3,
    first_name: 'Hindi',
  },
  {
    id: 4,
    first_name: 'Tamil',
  },

];

let selectedArray=[];
// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

const LanguageScreen = ({navigation}) => {
  const [data, setData] = React.useState({
    selected: null,
    renderData:Data,
    count:0,
    activate:false
  });

  const onPressHandler = (id) => {
    let renderData=data.renderData;
    for(let data of renderData){
          if(data.id==id){
            data.selected=(data.selected==null)?true:!data.selected;
            if(data.selected){
              data.count=data.count+1
            }
            else{
            data.count=data.count-1
            }
            if(data.count>2)
            {
              data.activate=true
            }else
            {
              data.activate=false
            }
            break;
          }
        }
        setData({...data,
          renderData:renderData,})
      }


    return (
      <View>
         
          <View style={styles.containerinner1}>
              <TouchableOpacity
              onPress={() => navigation.navigate('selectorscreen')}>
          <Text style={styles.intrest1}>
                     SKIP </Text>
                     </TouchableOpacity>
                     </View>
           <View style={styles.containerinner}>
                  <Text style={styles.intrest}>
                     Select languages to get  </Text>
                     <Text style={styles.intrest2}>Super recommendations</Text>
                </View>
          <View style={{marginTop:60,marginLeft:8}}>      
          <FlatList
          data= {data.renderData}
          keyExtractor={item => item.id.toString()}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          // style={{flexWrap:'wrap'}}
          // numRows={2}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => onPressHandler(item.id)}>
              <Text
                style={
                  item.selected== true
                    ? {
                      padding: 10,
                      borderRadius: 25,
                      backgroundColor: '#c30414',
                      // justifyContent:'space-between',
                      borderWidth:1.5,
                      // width:'69%',
                      marginLeft:25,
                      fontSize:20,
                      borderColor:'#c30414',
                      color:'white'
                        
                    }
                    : {
                      padding: 10,
                      borderRadius: 25,
                      backgroundColor: 'white',
                      // justifyContent:'space-between',
                      borderWidth:1.5,
                      // width:'69%',
                      marginLeft:25,
                      fontSize:20,
                      borderColor:'#c30414',
                      color:'#c30414'
                      }
                }>

                    {/* <Card
                      containerStyle={item.selectedItem === true ? {
                        padding: 10,
                        borderRadius: 25,
                        backgroundColor: '#c30414',
                        // justifyContent:'space-between',
                        borderWidth:1.5,
                        // width:'45%',
                        marginLeft:25,
                        borderColor:'#c30414',
                                } : {padding: 10,
                                  borderRadius: 25,
                                  backgroundColor: 'white',
                                  // justifyContent:'space-between',
                                  borderWidth:1.5,
                                  // width:'69%',
                                  marginLeft:25,
                                  fontSize:20,
                                  borderColor:'#c30414',}}>
                                <Text>{item.first_name}</Text>
                            </Card> */}
                  
                <Text style={{fontSize:17}}>{item.first_name}</Text>
              </Text>
            </TouchableOpacity>
          )}
        />
        </View>
            <TouchableOpacity
            style={styles.buttonStyle}
            disabled={data.activate}
            onPress = {() => navigation.navigate('selectorscreen')}
           >
            <Text style={styles.buttonTextStyle}>
              N E X T
            </Text>
          </TouchableOpacity>
            
       
      </View>
    );
}

const styles = StyleSheet.create({
    containerinner:{
        // flex:1,
        // flexDirection:'row'
      },
      containerinner1:{
        // flex:1,
        flexDirection:'row'
      },
    intrest:{
        marginTop:50,
        marginLeft:50,
        alignItems:"center",
        fontSize:30,
        fontWeight:"bold"
      },
      intrest2:{
        // flexDirection:'row',
        fontSize:30,
        marginLeft:40,
        marginBottom:10,
        alignItems:"center",
        fontSize:30,
        fontWeight:"bold",
        
      },
      buttonStyle: {
        minWidth: '90%',
        padding: 10,
        width:'90%',
        minHeight:"7%",
        backgroundColor: '#c30414',
        margin:"6.1%",
        borderRadius:10,
        marginTop: '75%',
        },
      buttonTextStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize:15,
        marginTop:5
      },
      intrest1:{
          // flexDirection:'row',
          marginLeft:340,
          color:"#c30414",
          fontWeight:"bold",
          fontSize:20,
          marginTop:10
      },
      tinyLogo: {
        marginTop:10,
        width: 30,
        height: 30,
      },
});

export default LanguageScreen;