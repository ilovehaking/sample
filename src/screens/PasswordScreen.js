import React, { useState } from 'react'
import { Text, TouchableOpacity, TextInput, View, SafeAreaView, StyleSheet, Image} from 'react-native'

const passwordScreen = ({navigation}) => {

    const[isFocused, setFocused] = useState(false)
    const[isValid, setValid] = useState(false)
    const[showPassword, setShowPassword] = useState(true)

    return(
        <SafeAreaView style={{flex: 1}}>
            <View style={{flex: 1}}>
                <TouchableOpacity onPress = {() => navigation.goBack() }>
                    <Image
                        source={require('./../../images/back.png')}
                        resizeMode='contain'
                        style={styles.img}
                    />
                </TouchableOpacity>
                <Text style={{fontSize: 25, textAlign: 'center', marginTop: 50, marginBottom: 40}}>Set your password</Text>
                <View style={isFocused ? styles.inputBoxFocused : styles.inputBox}>
                    <TextInput
                        placeholder='Enter password here'
                        selectionColor='#c51315'
                        style={{fontSize: 18, paddingStart: 20, flex: 1}}
                        onFocus={() => setFocused(true)}
                        onBlur={() => setFocused(false)}
                        secureTextEntry={showPassword}
                        onChangeText={(text) => text.length > 7 ? setValid(true) : setValid(false)}
                    />
                    <TouchableOpacity onPress={() => showPassword ? setShowPassword(false) : setShowPassword(true)}>
                        <Image
                            resizeMode='contain'
                            style={{marginEnd: 10, height: 25, width: 25, marginTop: 12}}
                            source={showPassword ? require('./../../images/show.png') : require('./../../images/hide.png')}
                        />
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10, marginStart: 10}}>
                    <Image
                        source={isValid ? require('./../../images/right.png') : require('./../../images/cross.png')}
                        resizeMode='contain'
                        style={{height: 25, width: 25}}
                    />
                    <Text style={{fontSize: 16, marginStart: 10}}>{isValid ? "Password is correct" : "Password is incorrect"}</Text>
                </View>
            </View>
            <View style={{flex: 1}}>
                <TouchableOpacity style={isValid ? styles.nextButton : styles.nextButtonInactive} onPress={() => navigation.navigate('languagescreen')} disabled={!isValid}>
                    <Text style={{color: '#fff'}}>N E X T</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    inputBox:{
        borderRadius: 5,
        borderColor: '#cdcdcd',
        borderWidth: 2,
        marginHorizontal: 10,
        flexDirection: 'row'
    },
    inputBoxFocused:{
        borderRadius: 5,
        borderColor: '#c51315',
        borderWidth: 2,
        marginHorizontal: 10,
        flexDirection: 'row'

    },
    img:{
        height: 20,
        width: 20,
        margin: 15
    },  
    nextButton:{
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: '#c51315',
        paddingVertical: 15,
        marginHorizontal: 10
      },
      nextButtonInactive:{
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: '#c51315',
        paddingVertical: 15,
        marginHorizontal: 10,
        opacity: 0.5
    
      }
    
})

export default passwordScreen;

