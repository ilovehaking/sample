import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  TouchableOpacity,
  Image,
  FlatList,
  useState
} from 'react-native';

import BottomBar from './BottomBar';
const homescreen =({navigation}) => {

  return (
    // <SafeAreaView>
      <View style = {styles.container1}>
      <View style = {styles.header}>
         
          <TouchableOpacity onPress={() => navigation.navigate('podcastscreen')}>
          <Image style={styles.back}  source={require('./../../images/notify.png')} resizeMode='contain'/>
          </TouchableOpacity>
        
          </View>
    
    <ScrollView>
    <View style={{flex:1,paddingTop : 20}}>
                        <View style = {styles.LeftTextView}>
                            <Text style ={styles.LeftText}>
                                Top podcasts 
                            </Text>
                            <View style = {styles.ViewAll}>
                            <TouchableOpacity onPress={() =>  navigation.navigate('newpodcastreleasescreen')}>
                              <Text style = {{fontSize : 13,fontWeight:'500',color:'black'}}>VIEW ALL</Text>
                            </TouchableOpacity>
                            </View>
                            </View>
                            <View style = {{ height: 130, marginTop: 20 }}>

                                  <FlatList 
    horizontal={true} 
    showsHorizontalScrollIndicator={false} 
    data={[  
      {key: 'https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png'},
      {key: 'https://thefader-res.cloudinary.com/private_images/w_760,c_limit,f_auto,q_auto:best/the-weeknd-starboy-no-1_erufgn/the-weeknd-starboy-every-song-charting-hot-100-drake.jpg'},
      {key: 'https://upload.wikimedia.org/wikipedia/en/3/3e/Earth_Song_cover.jpg'} 
  ]}
    renderItem={ ({ item, index }) => (
      <View style = {styles.BlockStyle}>
        <TouchableOpacity style={styles.imageStyle}>
         <Image source={{uri: item.key}} style={styles.imageStyle}></Image> 
         </TouchableOpacity>             
      </View>
    )}
  />
                            </View>
                        </View>
                        <View style={{flex:1,paddingTop : 20}}>
                        <View style = {styles.LeftTextView}>
                            <Text style ={styles.LeftText}>
                                Super Special Music
                            </Text>
                            <View style = {styles.ViewAll}>
                            <TouchableOpacity>
                              <Text style = {{fontSize : 13,fontWeight:'500',color:'black'}}>VIEW ALL</Text>
                            </TouchableOpacity>
                            </View>
                            </View>
                            <View style = {{ height: 130, marginTop: 20 }}>
                            <FlatList 
    horizontal={true} 
    showsHorizontalScrollIndicator={false} 
    data={[ 
      {key: 'https://www.clipartkey.com/mpngs/m/101-1013918_transparent-bts-wings-png-bts-wings-album-cover.png'} , 
      {key: 'https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png'},
      {key: 'https://thefader-res.cloudinary.com/private_images/w_760,c_limit,f_auto,q_auto:best/the-weeknd-starboy-no-1_erufgn/the-weeknd-starboy-every-song-charting-hot-100-drake.jpg'},
      {key: 'https://upload.wikimedia.org/wikipedia/en/3/3e/Earth_Song_cover.jpg'} 
  ]}
    renderItem={ ({ item, index }) => (
      <View style = {styles.BlockStyle1}>
        <TouchableOpacity style={styles.imageStyle}>
        <Image source={{uri: item.key}} style={styles.imageStyle}></Image>

        </TouchableOpacity>
                       
      </View>
    )}
  />
                            </View>
                        </View>

                        <View style={{flex:1,paddingTop : 20}}>
                        <View style = {styles.LeftTextView}>
                            <Text style ={styles.LeftText}>
                                New releases for you
                            </Text>
                            <View style = {styles.ViewAll}>
                            <TouchableOpacity>
                              <Text style = {{fontSize : 13,fontWeight:'500',color:'black'}}>VIEW ALL</Text>
                            </TouchableOpacity>
                            </View>
                            </View>
                            <View style = {{ height: 130, marginTop: 20 }}>
                            <FlatList 
    horizontal={true} 
    showsHorizontalScrollIndicator={false} 
    data={[  
      {key: 'https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png'},
      {key: 'https://thefader-res.cloudinary.com/private_images/w_760,c_limit,f_auto,q_auto:best/the-weeknd-starboy-no-1_erufgn/the-weeknd-starboy-every-song-charting-hot-100-drake.jpg'},
      {key: 'https://upload.wikimedia.org/wikipedia/en/3/3e/Earth_Song_cover.jpg'},
      {key: 'https://www.clipartkey.com/mpngs/m/101-1013918_transparent-bts-wings-png-bts-wings-album-cover.png'} 
  ]}
    renderItem={ ({ item, index }) => (
      <View style = {styles.BlockStyle1}>
        <TouchableOpacity style={styles.imageStyle}>
         <Image source={{uri: item.key}} style={styles.imageStyle}></Image>     
         </TouchableOpacity>         
      </View>
    )}
  />
                            </View>
                        </View>

                    </ScrollView>
                    <BottomBar></BottomBar>
                    </View>
                    
                   

 

  );

};

export default homescreen;
const styles = StyleSheet.create({
  container1: {
    flex :1,
    backgroundColor : '#f2eeee',
  },
  back:{
    height: 40,
    width: 40,
    marginRight:10,
    marginTop:5
  
   },
  imageStyle : {
    flex:1,
    width:null,
    height:null,
    resizeMode:'cover',
    borderRadius:10
  },
  BlockStyle : {height: 130, 
    backgroundColor : 'white',
    width: 300,
    marginLeft: 20,
    borderColor: '#dddddd',
    borderRadius:10
  },
    BlockStyle1 : {height: 130, 
      backgroundColor : 'white',
      width: 130,
      marginLeft: 20,
      borderColor: '#dddddd',
      borderRadius:10
    },
    LeftText : {flex:1,alignItems:"flex-start",
    color:'black',
    fontSize : 20,
    marginRight:18,
    fontWeight:'bold'
  },
  header : {
      flexDirection:'row',
      height:52,
     justifyContent:'flex-end',
      backgroundColor:'#D11515'
},
  ViewAll : {alignItems:'center',
  justifyContent:'center',

  marginRight:10,
  borderRadius:15,
  width:80,
  height:30
},
LeftTextView : {flex:1,alignItems:'center',
justifyContent:'center',
marginLeft:20,
flexDirection:'row'
}
});
