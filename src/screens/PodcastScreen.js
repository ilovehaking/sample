import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
} from 'react-native';
import Timedisplay from '../Components/Timedisplay';


const PodcastScreen =({navigation}) =>  {
  const [calls, state ] = useState( [

        {id:1,  description: "Lorem ipsum is an script and is tudesnjsdbc",   episodetitle:"10. Episode Title", status:"NEW RELEASE", image:"https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png"},
        {id:2,  description: "Lorem ipsum is an script and is tudesnjsdbc",   episodetitle:"172. Episode Title",status:"NEW RELEASE", image:"https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png"} ,
        {id:3,  description:"Lorem ipsum is an script and is tudesnjsdbc",  episodetitle:"06. Episode Title",status:"NEW RELEASE", image:"https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png"} ,
       
      ]);
  
      



    return(
      <View style={styles.container} >
      
        <View style={styles.notify}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image style={styles.back}  source={require('./../../images/close.png')} resizeMode='contain'/>
        </TouchableOpacity>
          <Text style={styles.item}> Notifications</Text>

        </View>
        <View style={styles.timecontainer}> 
          <Timedisplay/>
          </View>
       
        <FlatList 
          extraData={state}
          data={calls}
          keyExtractor = {(item) => {
            return item.id;
          }}
          renderItem={ ({item}) => {
            return (
              
                <View style={styles.row}>
                  <Image source={{ uri: item.image }} style={styles.pic} />
                  <View>
                  <View style={styles.msgContainer}>
                      <Text style={styles.msgTxt}>{item.status}</Text>
                    </View>
                    <View style={styles.msgContainer}>
                      <Text style={styles.msgTxt2}>{item.episodetitle}</Text>
                    </View>
                    <View style={styles.nameContainer}>
                      <Text style={styles.nameTxt} numberOfLines={1} ellipsizeMode="tail">{item.description}</Text>
<TouchableOpacity>
<Text style={styles.mblTxt}>more</Text>
</TouchableOpacity>
                    </View>
                 
                  </View>
                </View>
            
            );
          }}
          ItemSeparatorComponent={() => {  
            return (  
                <View  
                    style={{  
                        height: 23,  
                        width: "100%",  
                        backgroundColor: "#f6f6f6",  
                    }}  
                />  
            );  
        } }  />
      </View>
    );
  
}

const styles = StyleSheet.create({
  container: {
  flex:1,
  backgroundColor:'#fff'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#868F8F',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    padding: 10,
  },
  notify: {
    flex:0.28,
    flexDirection:'row',
    backgroundColor: '#D11515',
    
  },
  timecontainer:{
   justifyContent:'flex-start'
    
  },
  item: { 
    marginLeft:75,
    marginTop: 9,
    textAlign: 'center',
    alignItems:'center',
    paddingTop: 6,
    color: 'white',
    fontSize: 23,
   
},
back:{
  marginTop:10,
  marginLeft:18,
  height: 37,
  width: 37,

 },
  pic: {
    borderRadius: 10,
    width: 90,
    height: 85,
  
  },
  nameContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 300,
  },
  nameTxt: {
    marginLeft: 15,
    fontWeight: '600',
    color: '#222',
    fontSize: 18,
    width:230,
  },
  mblTxt: {
    fontWeight: '500',
    color: 'red',
    fontSize: 17,
  },
  msgContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  msgTxt: {
    fontWeight: 'bold',
    fontFamily:"sans-serif",
    color: 'red',
    fontSize: 14,
    marginLeft: 15,
  },
  msgTxt2: {
    fontWeight: 'bold',
    fontFamily:"sans-serif",
    color: 'black',
    fontSize: 18,
    marginLeft: 15,
  },
});
export default PodcastScreen;