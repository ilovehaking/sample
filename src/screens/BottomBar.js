import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  TouchableOpacity,
  Image,
  FlatList,
  useState
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import homescreen from './HomeScreen';
import emailScreen from './EmailScreen';
import playlist from './Search';
import PlayerScreen from './PlayerScreen';




const Tab = createBottomTabNavigator();

const BottomBar = () => 
{
  const navigation = useNavigation(); 
  return (
    <View style={styles.containerMain}>
        
        <View flexDirection ={'row'}
        backgroundColor = {'#000000'}
        style={styles.bottomView}>
         
          <TouchableOpacity onPress={() => navigation.navigate('podcastscreen')}>
          <Image style={{marginLeft:45,marginTop:5}} source={require('./../../images/podcast.png')}/>
          <Text style={{marginLeft:40,color:'white'}}>Podcast</Text>
          </TouchableOpacity>
          
          <TouchableOpacity onPress={() => navigation.navigate('playlist')}>
          <Image  style={{marginLeft:55,marginTop:5}} source={require('./../../images/music.png')}/>
          <Text style={{marginLeft:55,color:'white'}}>Music</Text>
          </TouchableOpacity>
          
          <TouchableOpacity onPress={() => navigation.navigate('playlist')}>
          <Image style={{marginLeft:60,marginTop:5}} source={require('./../../images/library.png')}/>
          <Text style={{marginLeft:55,color:'white'}}>Library</Text>
          </TouchableOpacity>
          
          <TouchableOpacity onPress={() => navigation.navigate('playlist')}>
          <Image style={{marginLeft:60,marginTop:5}} source={require('./../../images/contests.png')}/>
          <Text style={{marginLeft:47,color:'white'}}>Contests</Text>
          </TouchableOpacity>
         
          
        </View>
      </View>
  );
}
export default BottomBar;

const styles = StyleSheet.create({
  containerMain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomView: {
    width: '100%',
    height: 70,
    borderTopLeftRadius:40,
    borderTopRightRadius:40,
    position: 'absolute', //Here is the trick
    bottom: 0, //Here is the trick
  },
  textStyle: {
    color: '#fff',
    fontSize: 18,
  },
});
