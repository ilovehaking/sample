import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
} from 'react-native';
import homescreen from './HomeScreen';
import Playlist from './Search';

const newpodcastreleasescreen =({navigation}) => {
 
  const [data, state ] = useState( [
        {id:1, title: "Julie",  author:"CBC productions", image:"https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png"},
        {id:2, title: "The Drop Out",  author:"julie adams", image:"https://www.clipartkey.com/mpngs/m/101-1013918_transparent-bts-wings-png-bts-wings-album-cover.png"} ,
        {id:3, title: "Madam Adams",  author:"Julie adams", image:"https://upload.wikimedia.org/wikipedia/en/3/3e/Earth_Song_cover.jpg"}, 
        {id:4, title: "The London Street",  author:"CBC productions", image:"https://www.clipartkey.com/mpngs/m/101-1013918_transparent-bts-wings-png-bts-wings-album-cover.png"}, 
        {id:5, title: "Happy Sundays",  author:"CBC productions", image:"https://thefader-res.cloudinary.com/private_images/w_760,c_limit,f_auto,q_auto:best/the-weeknd-starboy-no-1_erufgn/the-weeknd-starboy-every-song-charting-hot-100-drake.jpg"}, 
        {id:6, title: "Product 6",  author:"CBC productions", image:"https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png"}, 
        {id:7, title: "Product 7",  author:"CBC productions", image:"https://upload.wikimedia.org/wikipedia/en/3/3e/Earth_Song_cover.jpg"}, 
        {id:8, title: "Product 8",  author:"CBC productions", image:"https://upload.wikimedia.org/wikipedia/en/3/3e/Earth_Song_cover.jpg"},
        {id:9, title: "Product 9",  author:"CBC productions", image:"https://upload.wikimedia.org/wikipedia/en/3/3e/Earth_Song_cover.jpg"},
        {id:9, title: "Product 10", author:"CBC productions", image:"https://via.placeholder.com/400x200/FA8072/000000"},
      ]
    );
  

 const addProductToCart = () => {
    Alert.alert('Success', 'The product has been added to your cart')
  }

  
    return (
      <View style={styles.container0}>
         
        <View style={styles.notify}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image style={styles.back}  source={require('./../../images/backarrow.png')} resizeMode='contain'/>
        </TouchableOpacity>
        <Text style={styles.item}> New Podcasts Releases</Text>
        <TouchableOpacity onPress={() => navigation.navigate('playlist')}>
            <Image style={styles.back}  source={require('./../../images/search.png')} resizeMode='contain'/>
        </TouchableOpacity>
        </View>
      <View style={styles.container}>
        <FlatList style={styles.list}
          contentContainerStyle={styles.listContainer}
          data={data}
          horizontal={false}
          numColumns={2}
          keyExtractor= {(item) => {
            return item.id;
          }}
          ItemSeparatorComponent={() => {
            return (
              <View style={styles.separator}/>
            )
          }}
          renderItem={(post) => {
            const item = post.item;
            return (
              <View style={styles.card}>
                <View style={styles.imageContainer}>
                  <TouchableOpacity  onPress={() => navigation.navigate('playerscreen')}>
                  <Image style={styles.cardImage} source={{uri:item.image}}/>
                  </TouchableOpacity>
                </View>
                <View style={styles.cardContent}>
                  <Text style={styles.title}>{item.title}</Text>
                  <Text style={styles.author}>{item.author}</Text>
                </View>
              </View>
            
            )
          }}/>
      </View>
      </View>
    );
  }

export default newpodcastreleasescreen;

const styles = StyleSheet.create({
  container0:{
    flex:1,
    backgroundColor:"white"
  },
  container:{
    flex:1,
    marginTop:20,
  },
  list: {
    paddingHorizontal: 10,
  },
  listContainer:{
    alignItems:'center'
  },
  notify: {
 flexBasis:'9%',
    flexDirection:'row',
    backgroundColor: '#D11515',
    
  },
  separator: {
    marginTop: 10,
  },
  /******** card **************/
  card:{
    marginVertical: 8,
  
    flexBasis: '45%',
    marginHorizontal: 10,
  },
  cardContent: {
    paddingVertical: 17,
    justifyContent: 'space-between',
  },
  item: { 
    marginLeft:30,
    marginTop: 9,
    textAlign: 'center',
    alignItems:'center',
    paddingTop: 6,
    color: 'white',
    fontSize: 23,
   
},
  cardImage:{
    flex: 1,
    height: 170,
    width: null,
    borderRadius:10,
  },
  imageContainer:{
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,

    elevation: 9,
  },
  back:{
    marginTop:10,
    marginLeft:18,
    height: 40,
    width: 37,
  
   },
  /******** card components **************/
  title:{
    fontSize:20,
    flex:1,
    fontWeight:'bold',
    color:"black"
  },
  author:{
    fontSize:18,
    flex:1,
    color:"#434747"
  },
});