import React, { useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const validator = require('validator')

const emailScreen = ({navigation}) => {

  const[isFocused, setFocused] = useState(false)
  const[isLoading, setLoading] = useState(false)
  const[text, setText] = useState()
  const[isValid, setValid] = useState(false);

  const goBack = () => {
    navigation.goBack()
  }

  const validateMail = (inp) => {
      validator.isEmail(inp) ? setValid(true) : setValid(false)
  }

  return(
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 0.5}}>
        <View>
          <TouchableOpacity onPress={() => goBack()}>
            <Image
                source={require('../../images/back.png')}
                resizeMode='contain'
                style={styles.img}
            />
          </TouchableOpacity>
          <View style={{alignItems: 'center', marginTop: 20}}>
              <Text style={styles.txtDesign}>Hi!</Text>
              <Text style={styles.txtDesign}>What's your email?</Text>
          </View>
          <View style={isFocused ? styles.textBoxFocus : styles.textBoxBlur}>
            <TextInput
              placeholder='Enter email here'
              style={{fontSize: 18, paddingStart: 20, flex: 1}}
              onChangeText={(inp) => (setText(inp), validateMail(inp))}
              onFocus={() => setFocused(true)}
              onBlur={() => setFocused(false)}
              keyboardType='email-address'
              selectionColor='#c51315'
            />
            <Image
                source={!isValid ? require('./../../images/cross.png') : require('./../../images/right.png')}
                resizeMode='contain'
                style={styles.img}
            />
          </View>
        </View>
      </View>
      <View style={{flex:0.5, paddingTop: 10}}>
          <TouchableOpacity style={isValid ? styles.nextButton : styles.nextButtonInactive} onPress={() => navigation.navigate('passwordscreen')} disabled={!isValid}>
            <Text style={{color: '#fff'}}>NE X T</Text>
          </TouchableOpacity>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  img:{
    height: 20,
    width: 20,
    margin: 15
  },
  txtDesign:{
    fontSize: 25
  },
  textBoxBlur:{
    borderRadius: 5,
    borderWidth: 2,
    borderColor: '#cdcdcd',
    marginTop: 40,
    marginHorizontal: 10,
    flexDirection: 'row'
  },
  textBoxFocus:{
    borderRadius: 5,
    borderWidth: 2,
    borderColor: '#c51315',
    marginTop: 40,
    marginHorizontal: 10,
    flexDirection: 'row'
  },
  spinner:{
    marginEnd: 10
  },
  nextButton:{
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: '#c51315',
    paddingVertical: 15,
    marginHorizontal: 10
  },
  nextButtonInactive:{
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: '#c51315',
    paddingVertical: 15,
    marginHorizontal: 10,
    opacity: 0.5

  }
});

export default emailScreen;
