import React,  {useState}  from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
} from 'react-native';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
} from 'react-native-popup-dialog';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
  

const selectorScreen =({navigation}) => {
  const [
    slideAnimationDialog, setSlideAnimationDialog
  ] = useState(false);


  const [data, setdata ] = useState( [
    {id:0, title: "Stories", image:"https://upload.wikimedia.org/wikipedia/en/3/3e/Earth_Song_cover.jpg",check:null},
    {id:1, title: "True Crime", image:"https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png",check:null},
    {id:2, title: "Sports", image: "https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png",check:null} ,
   
 
  ]);
  

  const  clickEventListener = (item) =>{
   /* var items=[...item]
    var newitem=[items[3]]
    newitem.check=require('./blackcircle.png')*/
    item.check=require('./../../images/blk-c.png')
   // Alert.alert(item.title)
  

  }
  const  onclickEventListener = () =>{
    
     setSlideAnimationDialog(true) 
     navigation.navigate('homescreen')
 
   }

    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image style={styles.back}  source={require('./../../images/back.png')} resizeMode='contain'/>
        </TouchableOpacity>
        <View style={styles.containerinner}>
                  <Text style={styles.intrest}>
                     What are you    </Text>
                     <Text style={styles.intrest2}>Super intrested in? </Text>
              
                </View>
        <FlatList style={styles.list}
          contentContainerStyle={styles.listContainer}
          data= {data}
          extraData={data}
          horizontal={false}
          numColumns={3}
          keyExtractor= {(item) => {
            console.log(item)
            return item.id;
          }}
        
          renderItem={({item}) => {
            return (
              <View>
                <View style={styles.circles}>
                
                <TouchableOpacity style={styles.card} onPress={() => {clickEventListener(item)}}>
                  <Image style={styles.cardImage} source={{uri:item.image}}/>
                </TouchableOpacity>
               
                <Image style={styles.checkimg}  source={item.check}/>
                </View>

                <View style={styles.cardHeader}>
                  <View style={{alignItems:"center", justifyContent:"center"}}>
                    <Text style={styles.title}>{item.title}</Text>
                  </View>
                </View>
               
              </View>
            )
          }}/>
          <TouchableOpacity   onPress={() =>{onclickEventListener()} }>
           <View style={styles.signup}>
                            <Text style={styles.item}> Next </Text>
                        </View>
                        </TouchableOpacity>
                        <Dialog
          onDismiss={() => {
            setSlideAnimationDialog(false);
          }}
          onTouchOutside={() => {
            setSlideAnimationDialog(false);
          }}
          visible={slideAnimationDialog}
         
        
      
          dialogAnimation={
            new SlideAnimation({slideFrom: 'bottom'})
          }>
          <DialogContent>
            <Text style={styles.insidedialog}>
              Creating for you
            </Text>
          <AnimatedCircularProgress style={styles.bar}
  size={85}
  width={8}
  fill={80}
  tintColor="#C40608"
  duration={3000}
 
  backgroundColor="#F0EDED" />
          </DialogContent>
        </Dialog>
      </View>
    );
  }


const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#f6f6f6',
  },
  insidedialog:{
  fontSize:30,
  marginBottom:20,
  marginTop:40,
  marginLeft:36,
  marginRight:40,
  },
  circles:{
   flex:1,
   justifyContent:'space-around'
    },
  bar:{
 alignItems:"center",
 paddingBottom:40
  },
  back:{
    marginTop:15,
    height: 20,
    width: 20,
    marginStart: 20
   },
  containerinner:{
    flex:1,
    height:50
    
  },
  checkimg:{
    position:"absolute",
    marginTop:12,
    marginLeft:101,
  },
  item: {
    marginTop: 5,
    textAlign: 'center',
    paddingTop: 4,
    marginLeft: 18,
    color: 'white',
    fontSize: 25,
   
},
  intrest:{
    marginTop:10,
    marginLeft:90,
    alignItems:"center",
    fontSize:36,
  },
  
  signup: {
    borderRadius: 2,
    marginLeft: 25,
    marginRight: 20,
    marginBottom: 30,
    width: 350,
    height: 60,
    paddingTop: 4,
  backgroundColor:"#D11515",
},
  intrest2:{
    fontSize:30,
    marginLeft:50,
    
    alignItems:"center",
    fontSize:36,
  },
  list: {
    
    marginBottom:15,
    paddingHorizontal: 5,
    backgroundColor:"#f6f6f6",
  },
  listContainer:{
    alignItems:'center',
  },
  /******** card **************/
  card:{
    
  marginBottom:8,
    marginHorizontal: 20,
    backgroundColor:"blue",
    //flexBasis: '42%',
    width:105,
    height:105,
    borderRadius:70,
    alignItems:'center',
    justifyContent:'center'
  },
  cardHeader: {
    paddingVertical: 1,
    paddingHorizontal: 10,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    alignItems:"center", 
    justifyContent:"center"
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
  },
  cardFooter:{
    flexDirection: 'row',
    justifyContent: 'space-between',
   
  },
  cardImage:{
    height: 105,
    width: 105,
    alignSelf:'center',
    borderRadius:52,
  },
  title:{
    fontSize:20,
    fontFamily: "sans-serif",
    marginBottom:20,
    flex:1,
    alignSelf:'center',
    color:"black"
  },
});    

export default selectorScreen;