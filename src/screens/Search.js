import React, { useState } from 'react';
import {
  FlatList,
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import Category3 from '../Components/Category3';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Header from '../Components/HeaderComponent';

const DATA = [
  { id: 1, title: 'Julie', description: 'CBC Podcasts ' },
  { id: 2, title: 'MLB Marathon', description: 'BBC Podcasts' },
  { id: 3, title: 'Buy Black', description: 'BBC Podcasts ' },
  { id: 4, title: 'CLM BLUES', description: 'CBC Podcasts ' },
];
const playlist = ({navigation}) => {
  const [selectedId, setSelectedId] = useState(null);
  
  const Item = ({ item, onPress, style }) => (
    <TouchableOpacity style={styles.card}>
      <Category3 />
      <View style={styles.cardContent}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.description}>{item.description}</Text>
      </View>
      <View>
       
      </View>
      <View />
    </TouchableOpacity>
  );
  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? '#6e3b6e' : '#f9c2ff';
    return (
      <Item
        item={item}
        onPress={() => setSelectedId(item.id)}
        style={{ backgroundColor }}
      />
    );
  };
  return (

    <View style={styles.container}>

      <Header  isSearch={true} />
      
      <View style={{ height: '15%' }}>
      <Text style = {{fontSize:25,marginLeft:20,paddingTop:30}}>Recent Searches</Text>

          <ScrollView horizontal={true}
          flexDirection ='row'
          showsHorizontalScrollIndicator = {false}> 
          <View marginTop={20} marginLeft={20}
          
          style = {{borderRadius:15,height:25}}>
              <TouchableOpacity style={{backgroundColor: '#FFFFFF',
    shadowColor: 'rgba(0, 0, 0, 0.1)',borderRadius:15,height:25,
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15 ,
    shadowOffset : { width: 1, height: 13}}}>
              <Text style={{fontSize:18}}> Monsoon x</Text>

              </TouchableOpacity>
              
          </View>
          <View marginTop={20} marginLeft={20}
          
          style = {{borderRadius:15,height:25}}>
              <TouchableOpacity style={{backgroundColor: '#FFFFFF',
    shadowColor: 'rgba(0, 0, 0, 0.1)',borderRadius:15,height:25,
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15 ,
    shadowOffset : { width: 1, height: 13}}}>
              <Text style={{fontSize:18}}> Monsoon x</Text>

              </TouchableOpacity>
              
          </View>
          <View marginTop={20} marginLeft={20}
          
          style = {{borderRadius:15,height:25}}>
              <TouchableOpacity style={{backgroundColor: '#FFFFFF',
    shadowColor: 'rgba(0, 0, 0, 0.1)',borderRadius:15,height:25,
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15 ,
    shadowOffset : { width: 1, height: 13}}}>
              <Text style={{fontSize:18}}> Monsoon x</Text>

              </TouchableOpacity>
              
          </View>
          <View marginTop={20} marginLeft={20}
          
          style = {{borderRadius:15,height:30}}>
              <TouchableOpacity style={{backgroundColor: '#FFFFFF',
    shadowColor: 'rgba(0, 0, 0, 0.1)',borderRadius:15,height:25,
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15 ,
    padding:4,
    shadowOffset : { width: 1, height: 13}}}>
              <Text style={{fontSize:18}}> Monsoon x</Text>

              </TouchableOpacity>
              
          </View>


          </ScrollView>
       
        <View  />
        
        
       
      </View>
      <Text style = {{fontSize:25,marginLeft:20,paddingTop:30}}>Suggestions For You</Text>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        extraData={selectedId}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
  },
 
  cardContent: {
    marginLeft: 20,
    marginTop: 10,
    height: '100%',
  },
  card: {
    width: '100%',
    height: '150%',
    
    backgroundColor: 'white',
    flexBasis: '20%',
    
    flexDirection: 'row',
 
  },
  title: {
    fontSize: 18,
 
    
    fontWeight: 'bold',
    marginTop: '10%',
  },
  description: {
    fontSize: 14,

    color: '#696969',
    marginBottom: '50%',
    marginTop: '10%',
  },
  menu: {
    marginLeft: '50%',
    marginTop: '10%'
  },
  menuContent: {
    color: 'black',
    padding: 2,
    fontSize: 15,
    marginLeft: 2,
    width: '43%',
    marginRight: "-10%"
  },

  
});
export default playlist;