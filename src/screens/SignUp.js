import {View, TouchableOpacity, Text, TextInput, Image, StyleSheet, ToastAndroid} from 'react-native';
import React, { useState } from 'react';

import DropDown from 'react-native-dropdown-picker';

//importing Temporary JSON data
const file = require('./../Data/JSONData/CountryData.json');

const signUp = ({navigation}) => {

    const [countryCode, setCountryCode] = useState('+91');
    const [phoneNumber, setPhoneNumber] = useState('');
 
    const [countryData, setCountryData] = useState(file.countries)

    const skipFunction = () => {
        navigation.navigate('emailscreen')
    }

    const connectToMail = () => {
        alert('connect to mail')
    }

    const connectToFacebook = () => {
        alert('connect to facebook')
    }

    const moveToNext = () => {
        if(phoneNumber === ''){
            ToastAndroid.show('Cannot leave field empty', ToastAndroid.SHORT);
        }else{
            navigation.navigate('otpscreen', {
                phoneNumber: `${countryCode}${phoneNumber}`,
                otp: Math.round(Math.random() * 1000000)
            });
        }
    }

    return(
        <View style={{flex: 1, backgroundColor: '#fff'}}>
            <Image
                resizeMode='contain'
                source={{uri: 'https://pbs.twimg.com/profile_images/625222250266886146/Yy_mu4-H_400x400.jpg'}}
                style={styles.appIcon}
            />
            <View style={styles.unit2}>
                <View style={{borderRadius: 10, borderColor: '#cdcdcd', marginHorizontal: 10, borderWidth: 1.5}}>
                    <View style={{marginVertical: 15, marginStart: 10}}>
                        <DropDown
                            items={countryData}
                            defaultIndex={0}
                            containerStyle={{height: 30, padding: 0, marginEnd: 10}}
                            itemStyle={{alignItems: 'flex-start', justifyContent: 'flex-start'}}
                            labelStyle={{marginEnd: 120, marginVertical: 0, fontSize: 16}}
                            style={{margin: 0, borderWidth: 0, padding: 0}}
                            onChangeItem={(val) =>  setCountryCode(val.value)}
                        />
                    </View>
                    <View style={{flexDirection: 'row', borderTopColor: '#cdcdcd', borderTopWidth: 1.5}}>
                        <Text style={{paddingTop: 15, marginStart: 15}}>{countryCode}</Text>
                        <TextInput
                            editable
                            keyboardType='numeric'
                            placeholder='Enter mobile number'
                            style={styles.phoneNumber}
                            onChangeText={(v) => setPhoneNumber(v)}
                        />
                    </View>
                </View>
                <Text style={{marginStart: 20, marginTop: 15, fontSize: 16}}>You will receive an OTP on this number</Text>
            </View>
            <TouchableOpacity style={styles.nextButton} onPress={() => moveToNext()}>
                <Text style={{textAlign: 'center', color: '#fff'}}>N E X T</Text>
            </TouchableOpacity>
            <View style={{flex: 1.3}}>
                <Text style={{textAlign: 'center', marginVertical: 10, fontWeight: 'bold'}}>OR</Text>
                <TouchableOpacity style={styles.connectToNetwork} onPress={() => connectToFacebook()}>
                    <Image
                        source={require('./../../images/fb.png')}
                        resizeMode='contain'
                        style={{height: 25, width: 25, marginStart: 10}}
                    />
                    <Text style={{textAlign: 'center', flex: 1, marginTop: 2.5, marginEnd: 25}}>Connect to Facebook</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.connectToNetwork} onPress={() => connectToMail()}>
                    <Image
                        source={require('./../../images/mail.png')}
                        resizeMode='contain'
                        style={{height: 25, width: 25, marginStart: 10}}
                    />
                    <Text style={{textAlign: 'center', flex: 1, marginTop: 2.5, marginEnd: 25}}>Connect to mail</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{alignItems: 'center', marginTop: 10}} onPress={() => skipFunction()}>
                    <Text style={{fontSize: 16, fontWeight: 'bold', color: '#c51315'}}>SKIP</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    appIcon:{
        flex: 1,
        margin: 10
    },
    unit2:{
        flex: 1
    },
    unit3:{
        flex: 1
    },
    phoneNumber:{
        fontSize: 16,
        paddingStart: 10,
        flex: 1
    },
    nextButton:{
        backgroundColor: '#c51315',
        marginHorizontal: 10,
        paddingVertical: 15,
        borderRadius: 10
    },
    connectToNetwork:{
        textAlign: 'center',
        flexDirection: 'row',
        paddingVertical: 15,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#cdcdcd',
        marginHorizontal: 10,
        marginTop: 10
    }
})

export default signUp