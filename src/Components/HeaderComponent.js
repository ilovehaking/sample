import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
  TextInput,
  Keyboard,
  StatusBar,
  Text,
  Image,
} from 'react-native';
import {Badge, Icon} from 'react-native-elements';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Color from '../Components/colors';
import Voice from '@react-native-community/voice';
console.disableYellowBox = true;
const {width} = Dimensions.get('window');
export default function HeaderComponent(props) {
  //Mic Start
  const [micText, setMicText] = useState('');
  const [voiceState, setVoiceState] = useState({
    recognized: '',
    pitch: '',
    error: '',
    end: '',
    started: '',
    results: [],
    partialResults: [],
  });
  useEffect(() => {
    Voice.onSpeechStart = onSpeechStart;
    Voice.onSpeechRecognized = onSpeechRecognized;
    Voice.onSpeechEnd = onSpeechEnd;
    Voice.onSpeechError = onSpeechError;
    Voice.onSpeechResults = onSpeechResults;
    Voice.onSpeechPartialResults = onSpeechPartialResults;
    Voice.onSpeechVolumeChanged = onSpeechVolumeChanged;
    Voice.destroy().then(Voice.removeAllListeners);
  }, []);
  const onSpeechStart = (e) => {
    console.log('onSpeechStart: ', e);
    setVoiceState({...voiceState, started: '√'});
  };
  const onSpeechRecognized = (e) => {
    console.log('onSpeechRecognized: ', e);
    setVoiceState({
      ...voiceState,
      recognized: '√',
    });
  };
  const onSpeechEnd = (e) => {
    console.log('onSpeechEnd: ', e);
    setVoiceState({
      ...voiceState,
      end: '√',
    });
  };
  const onSpeechError = (e) => {
    console.log('onSpeechError: ', e);
    setVoiceState({
      ...voiceState,
      error: JSON.stringify(e.error),
    });
  };
  const onSpeechResults = (e) => {
    console.log('onSpeechResults: ', e);
    if (e.value.length > 1) {
      setMicText(e.value[1]);
    }
    console.log('onSpeechResults:121 ' + e.value.length);
    setVoiceState({
      ...voiceState,
      results: e.value,
    });
  };
  const onSpeechPartialResults = (e) => {
    console.log('onSpeechPartialResults: ', e);
    setVoiceState({
      ...voiceState,
      partialResults: e.value,
    });
  };
  const onSpeechVolumeChanged = (e) => {
    console.log('onSpeechVolumeChanged: ', e);
    setVoiceState({
      ...voiceState,
      pitch: e.value,
    });
  };
  const _startRecognizing = async () => {
    Keyboard.dismiss();
    setMicText('');
    setVoiceState({
      ...voiceState,
      recognized: '',
      pitch: '',
      error: '',
      started: '',
      results: [],
      partialResults: [],
      end: '',
    });
    try {
      await Voice.start('en-US', {
        EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS: 5000,
      });
    } catch (e) {
      console.error(e);
    }
  };
  const _stopRecognizing = async () => {
    try {
      await Voice.stop();
    } catch (e) {
      console.error(e);
    }
  };
  const _cancelRecognizing = async () => {
    try {
      await Voice.cancel();
    } catch (e) {
      console.error(e);
    }
  };
  const _destroyRecognizer = async () => {
    try {
      await Voice.destroy();
    } catch (e) {
      console.error(e);
    }
    setVoiceState({
      ...voiceState,
      recognized: '',
      pitch: '',
      error: '',
      started: '',
      results: [],
      partialResults: [],
      end: '',
    });
  };
  //mic End
  const checkTitle = (isSearch) => {
    if (isSearch) {
      return (
        <View style={props.isSearch ? styles.innerview : styles.innerview2}>
          <Image
            style={styles.tinyLogo1}
            source={{
              uri:
                'https://cdn0.iconfinder.com/data/icons/30-hardware-line-icons/64/Search-512.png',
            }}
          />
          <TextInput
            placeholder="Search Podcasts..."
            style={styles.textinput}
            defaultValue={micText}
          />
          <TouchableOpacity onPress={_startRecognizing}>
            <Image
              style={styles.mic2}
              source={{
                uri:
                  'https://cdn4.iconfinder.com/data/icons/symbol-blue-set-1/100/Untitled-2-80-512.png',
              }}
            />
          </TouchableOpacity>
        </View>
      );
    } else {
      return <Text style={styles.title}> {props.title} </Text>;
    }
  };
  return (
    <SafeAreaView style={styles.SafeAreaStyle}>
      <StatusBar backgroundColor={Color.APP_RED} />
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <View style={styles.menuButton}>
            <TouchableOpacity
              onPress={() => {
                //this.props.navigation.navigate('DrawerOpen');
                Keyboard.dismiss();
                props.navigation.openDrawer();
              }}>
              <Image
        style={styles.menuOptions}
        source={{
          uri: 'https://www.flaticon.com/svg/static/icons/svg/966/966313.svg',
        }}
      />
              
            </TouchableOpacity>
          </View>
          <View>{checkTitle(props.isSearch)}</View>
          <View style={styles.container}>
            <View style={styles.row}>
              <TouchableOpacity
                onPress={() => {
                  props.navigation.navigate('Notification');
                }}>
                <Image
                  style={styles.notfication}
                  source={{
                    uri:
                      'https://img.icons8.com/ios/452/appointment-reminders.png',
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
          
          <View style={styles.user} size={30}></View>
        </View>
        <View style={styles.menuButton} />
      </View>
     
    </SafeAreaView>
  );
}
const styles = {
  headerContainer: {
    height: 70,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#c30414',
  },
  headerTitle: {
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    color: 'white',
  },
  menuButton: {
    alignSelf: 'center',
    tintColor: 'white',
    position: 'relative',
    marginLeft: 50,
  },
  menuContainer: {
    flex: 1,
    backgroundColor: '#c30414',
  },
  menuTitleContainer: {
    alignItem: 'center',
    height: 60,
    width: '80%',
    flexDirection: 'row',
  },
  menuTitle: {
    width: '100%',
    color: 'white',
    textAlign: 'center',
    fontSize: 17,
    alignSelf: 'center',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: -30,
    // marginRight: 20,
    width: '5%',
    // marginTop:10
  },
  text: {
    fontSize: 1,
  },
  row: {
    // flexDirection: 'row',
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 120,
    height: 25,
    width: 50,
    marginBottom: 15,
    // paddingRight: 50
    marginRight: 50,
  },
  //   badgeStyle: {
  //     position: 'absolute',
  //     top: -4,
  //     right: 20,
  //   },
  textinput: {
    height: '78%',
    fontSize: 15,
    marginTop: 7,
    marginBottom: 20,
    marginLeft: 15,
    width: '65%',
    backgroundColor: 'white',
    // fontStyle: 'italic',
    borderRadius: 35,
    opacity: 1,
  },
  innerview: {
    marginTop: 10,
    marginLeft: '60%',
    marginRight: 20,
    borderWidth: 1,
    //borderColor: '#f5f5f5',
    // borderBottomWidth: 2,
    borderColor: '#dcdcdc',
    // backgroundColor: '#dcdcdc',
    flexDirection: 'row',
    //borderRadius: 10,
    width: '60%',
    height: '70%',
    backgroundColor: 'white',
    marginBottom: 5,
    borderRadius: 15,
    opacity: 1,
  },
  title: {
    flex: 1,
    fontSize: 25,
    marginTop: 18,
    marginBottom: 5,
    marginLeft: 35,
    marginRight: 20,
    flexDirection: 'row',
    width: 245,
    height: 50,
    fontWeight: 'bold',
    color: 'white',
    alignItems: 'center',
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  innerview2: {
    //flex:0.25,
    marginTop: 10,
    marginLeft: 25,
    marginRight: 20,
    borderWidth: 1,
    // borderColor: '#f5f5f5',
    // borderBottomWidth: 2,
    borderColor: '#dcdcdc',
    // backgroundColor: '#dcdcdc',
    flexDirection: 'row',
    // borderRadius: 10,
    //   width:245,
    height: 0,
    backgroundColor: 'white',
    marginBottom: 5,
    borderRadius: 35,
    opacity: 0,
  },
  // img: {
  //     marginTop: 10,
  //     marginLeft: 60,
  //     height: 30,
  //     width: 30,
  //     marginBottom: 10,
  //     // justifyContent: "space-between",
  // },
  menuOptions: {
    flexDirection: 'row',
    marginTop: 20,
    marginLeft: 350,
    height: '50%',
    width: '90%',
    marginBottom: 15,
    paddingLeft: 25,
    alignItems: 'center',
  },
  user: {
    flexDirection: 'row',
    marginRight: 410,
    width: '100%',
    height: '50%',
    marginBottom: 15,
    marginTop: 20,
    // paddingRight: 24,
    marginLeft: 20,
    // marginLeft:25,
  },
  // mic: {
  //   justifyContent: 'flex-end',
  //   alignSelf: 'flex-end',
  //   marginTop: 10,
  //   marginLeft:5,
  // },
  mic2: {
    // flexDirection:'row',
    alignSelf: 'flex-end',
    justifyContent: 'flex-end',
    width: 28,
    height: 26,
    marginTop: '40%',
    marginLeft: 10,
    marginRight: -14,
  },
  tinyLogo1: {
    flexDirection: 'row',
    width: 23,
    height: 23,
    marginTop: '4%',
    marginLeft: 5,
  },
  notfication: {
    width: 30,
    height: 30,
    justifyContent: 'flex-end',
    alignContent: 'flex-end',
    marginLeft: 125,
  },
  // menu:{
  //   width:60,
  //   height:60,
  //   marginTop:-7,
  //   justifyContent:'flex-start',
  //   alignContent:'flex-start',
  //   marginLeft:320
  // }
  // text: {
  //     position: 'absolute'
  // }
};