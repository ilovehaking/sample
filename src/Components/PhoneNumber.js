import React, { useState } from 'react'
import {Text, TextInput, StyleSheet, View} from 'react-native'


const phoneInput = ({hint, getNumber}) => {

    const [data, setData]  = useState([{
        value: 'Banana',
      }, {
        value: 'Mango',
      }, {
        value: 'Pear',
      }])

    return(
        <View style={styles.main}>
            <View style={{width: 60,borderEndWidth: 1, borderRadius: 10 }}>
                <Text style={{textAlign: 'center', marginTop: 14}}>+91</Text>
            </View>
            <TextInput
                style={{flex: 1, paddingStart: 10}}
                placeholder={hint}
                keyboardType='numeric'
                textContentType='telephoneNumber'
                placeholderTextColor='#282828'
                onChangeText={input => getNumber(input)}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    main:{
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#212121',
        borderRadius: 10
    }
})

export default phoneInput