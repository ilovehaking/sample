
import React, {useState, useEffect} from 'react';

import { StyleSheet, View, Text} from 'react-native';


import moment from 'moment';

const Timedisplay= () => {
  const [currentDate, setCurrentDate] = useState('');

  useEffect(() => {
    var date = moment()
                  .utcOffset('+05:30')
                  .format(' DD MMM YYYY ');
    setCurrentDate(date);
  }, []);

  return (
  
        <View style={styles.container}>
        
          <Text style={styles.textStyle}>
              Today, 
            {currentDate}
          </Text>
        </View>
       
      
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E4EBEB',
    justifyContent: 'center',
    padding: 20,
  },
  textStyle: {
    fontSize: 16,
    color: 'black',
  },
});

export default Timedisplay;