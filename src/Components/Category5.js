import React, {Component} from 'react';
import {View, Text, StyleSheet, Image,TouchableOpacity} from 'react-native';

function Category5(props) {
  return (
    <TouchableOpacity>
    <View
      style={{
        height: 130,
        width: 110,
        marginLeft: 20,
        // borderWidth: 0.5,
        borderColor: '#dddddd',
        borderTopEndRadius: 10,
        borderTopLeftRadius: 10,
        // borderBottomEndRadius: 10,
        borderBottomLeftRadius: 10,
        // borderWidth: 0.5,
      }}>
      <Image
        source={{
          uri:
            'https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png',
        }}
        style={styles.imagestyle}/>
        <Text style={{marginLeft:"6.2%"}}>    {props.title}</Text>
    </View>
    </TouchableOpacity>

  );
}
export default Category5;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imagestyle:{
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover',
    borderRadius: 10,
  }
})