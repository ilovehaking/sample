import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

function Category3() {
  return (
    <View
      style={{
        height: 100,
        backgroundColor: 'white',
        width: 100,
        margin: '5%',
        marginRight: '-2%',
        borderWidth: 0.5,
        borderColor: '#dddddd',
        borderTopEndRadius: 10,
        borderTopLeftRadius: 10,
        borderBottomEndRadius: 10,
        borderBottomLeftRadius: 10,
        borderWidth: 0.5,
      }}>
      <Image
        source={{
          uri:
            'https://upload.wikimedia.org/wikipedia/en/3/32/Dua_Lipa_Swan_Song.png',
        }}
        style={{
          flex: 1,
          width: null,
          height: null,
          resizeMode: 'cover',
          borderRadius: 10,
        }}></Image>
    </View>
  );
}
export default Category3;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});