import 'react-native-gesture-handler';

import React from 'react';

import SignUpScreen from './src/screens/SignUp';
import OtpScreen from './src/screens/OTPScreen'
import EmailScreen from './src/screens/EmailScreen'
import PasswordScreen from './src/screens/PasswordScreen';
import LanguageScreen from './src/screens/LanguageScreen';
import SelectorScreen from './src/screens/SelectorScreen';
import homeScreen from './src/screens/HomeScreen';
import PodcastScreen from './src/screens/PodcastScreen';
import NewpodcastreleaseScreen from './src/screens/NewpodcastreleaseScreen';
import PlayerScreen from './src/screens/PlayerScreen';
import playlist from './src/screens/Search';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const App = () => {

	return(
		<NavigationContainer>
			<Stack.Navigator>
				<Stack.Screen name="signupscreen" component={SignUpScreen} options={{ headerShown: false}} />
				<Stack.Screen name="otpscreen" component={OtpScreen} options={{ headerShown: false}} />
				<Stack.Screen name='emailscreen' component={EmailScreen} options={{headerShown: false}} />
				<Stack.Screen name='passwordscreen' component={PasswordScreen} options={{headerShown: false}}/>
				<Stack.Screen name='languagescreen' component={LanguageScreen} options={{headerShown: false}}/>
				<Stack.Screen name='selectorscreen' component={SelectorScreen} options={{headerShown: false}}/>
				<Stack.Screen name='podcastscreen' component={PodcastScreen} options={{headerShown: false}}/>
				<Stack.Screen name='homescreen' component={homeScreen} options={{headerShown: false}}/>
				<Stack.Screen name='newpodcastreleasescreen' component={NewpodcastreleaseScreen} options={{headerShown: false}}/>
				<Stack.Screen name='playerscreen' component={PlayerScreen} options={{headerShown: false}}/>
				<Stack.Screen name='playlist' component={playlist} options={{headerShown: false}}/>
			</Stack.Navigator>
	  	</NavigationContainer>

	)
}

export default App;